Avvio Nodo:

java -jar Alchemist.jar -serv benchmark-config.xml

Avvio Client:

java -cp /path/to/classpath/with/dependencies:alchemist.jar it.unibo.alchemist.Alchemist -y "18-export.yml" -e "exported_data" -t "20" -b -bmk testB -d benchmark-config.xml -var "seed"